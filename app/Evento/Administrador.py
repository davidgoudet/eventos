# -*- coding: utf-8 -*-
from flask import request, session, Blueprint, json

Administrador = Blueprint('Administrador', __name__)


@Administrador.route('/administrador/AccionCrearEvento', methods=['POST'])
def AccionCrearEvento():
    #Access to POST/PUT fields using request.form['name']
    #Access to file fields using request.files['name']
    results = [{'label':'/VistaPrincipalAdmin', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@Administrador.route('/administrador/AccionEditarEvento', methods=['POST'])
def AccionEditarEvento():
    #Access to POST/PUT fields using request.form['name']
    #Access to file fields using request.files['name']
    results = [{'label':'/VistaPrincipalAdmin', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@Administrador.route('/administrador/AccionEliminarEvento')
def AccionEliminarEvento():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipalAdmin', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@Administrador.route('/administrador/AccionPrincipalAdmin')
def AccionPrincipalAdmin():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipalAdmin', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@Administrador.route('/administrador/VistaCrearEvento')
def VistaCrearEvento():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@Administrador.route('/administrador/VistaEditarEvento')
def VistaEditarEvento():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@Administrador.route('/administrador/VistaPrincipalAdmin')
def VistaPrincipalAdmin():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)





#Use case code starts here


#Use case code ends here