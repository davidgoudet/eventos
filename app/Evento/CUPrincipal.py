# -*- coding: utf-8 -*-
from flask import request, session, Blueprint, json

CUPrincipal = Blueprint('CUPrincipal', __name__)


@CUPrincipal.route('/cuprincipal/AccionCertificado')
def AccionCertificado():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipalUsuario', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/AccionCredencial')
def AccionCredencial():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipalUsuario', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/AccionEventoUsuario')
def AccionEventoUsuario():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaEventoUsuario', 'msg':[]}, {'label':'/VistaPrincipalUsuario', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/AccionPrincipalUsuario')
def AccionPrincipalUsuario():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipalUsuario', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/AccionReservar')
def AccionReservar():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipalUsuario', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/VistaCertificado')
def VistaCertificado():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/VistaCredencial')
def VistaCredencial():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/VistaEventoUsuario')
def VistaEventoUsuario():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/VistaPrincipalUsuario')
def VistaPrincipalUsuario():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@CUPrincipal.route('/cuprincipal/VistaReservar')
def VistaReservar():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)





#Use case code starts here


#Use case code ends here