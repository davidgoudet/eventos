# -*- coding: utf-8 -*-
from flask import request, session, Blueprint, json

Visitante = Blueprint('Visitante', __name__)


@Visitante.route('/visitante/AccionInicioSesion', methods=['POST'])
def AccionInicioSesion():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipalUsuario', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@Visitante.route('/visitante/AccionPrincipal')
def AccionPrincipal():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaPrincipal', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@Visitante.route('/visitante/AccionRegistro', methods=['POST'])
def AccionRegistro():
    #POST/PUT parameters
    params = request.get_json()
    results = [{'label':'/VistaInicioSesion', 'msg':[]}, ]
    res = results[0]
    #Action code goes here, res should be a list with a label and a message


    #Action code ends here
    if "actor" in res:
        if res['actor'] is None:
            session.pop("actor", None)
        else:
            session['actor'] = res['actor']
    return json.dumps(res)



@Visitante.route('/visitante/VistaInicioSesion')
def VistaInicioSesion():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@Visitante.route('/visitante/VistaPrincipal')
def VistaPrincipal():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)



@Visitante.route('/visitante/VistaRegistro')
def VistaRegistro():
    res = {}
    if "actor" in session:
        res['actor']=session['actor']
    #Action code goes here, res should be a JSON structure


    #Action code ends here
    return json.dumps(res)





#Use case code starts here


#Use case code ends here