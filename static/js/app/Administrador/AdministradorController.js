Module.config(function ($routeProvider) {
    $routeProvider.when('/VistaCrearEvento', {
                controller: 'VistaCrearEventoController',
                templateUrl: 'app/Administrador/VistaCrearEvento.html'
            }).when('/VistaEditarEvento', {
                controller: 'VistaEditarEventoController',
                templateUrl: 'app/Administrador/VistaEditarEvento.html'
            }).when('/VistaPrincipalAdmin', {
                controller: 'VistaPrincipalAdminController',
                templateUrl: 'app/Administrador/VistaPrincipalAdmin.html'
            });
});

Module.controller('VistaCrearEventoController', 
   ['$scope', '$location', '$route', 'flash', 'AdministradorService',
    function ($scope, $location, $route, flash, AdministradorService) {
      $scope.msg = '';
      $scope.evento = {};

      AdministradorService.VistaCrearEvento().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.eventoSubmitted = false;
      $scope.AccionCrearEvento0 = function(isValid) {
        $scope.eventoSubmitted = true;
        if (isValid) {
          
          AdministradorService.AccionCrearEvento($scope.evento).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              if (label == '/VistaCrearEvento') {
                  $route.reload();
              } else {
                  $location.path(label);
              }
          });
        }
      };

    }]);
Module.controller('VistaEditarEventoController', 
   ['$scope', '$location', '$route', 'flash', 'AdministradorService',
    function ($scope, $location, $route, flash, AdministradorService) {
      $scope.msg = '';
      $scope.evento = {};

      AdministradorService.VistaEditarEvento().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.eventoSubmitted = false;
      $scope.AccionEditarEvento0 = function(isValid) {
        $scope.eventoSubmitted = true;
        if (isValid) {
          
          AdministradorService.AccionEditarEvento($scope.evento).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              if (label == '/VistaEditarEvento') {
                  $route.reload();
              } else {
                  $location.path(label);
              }
          });
        }
      };

    }]);
Module.controller('VistaPrincipalAdminController', 
   ['$scope', '$location', '$route', 'flash', 'AdministradorService',
    function ($scope, $location, $route, flash, AdministradorService) {
      $scope.msg = '';
      AdministradorService.VistaPrincipalAdmin().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.AccionPrincipalAdmin0 = function() {
          
        AdministradorService.AccionPrincipalAdmin().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaPrincipalAdmin') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};
      $scope.VistaCrearEvento1 = function() {
        $location.path('/VistaCrearEvento');
      };
      $scope.VistaEditarEvento2 = function() {
        $location.path('/VistaEditarEvento');
      };
      $scope.AccionEliminarEvento3 = function() {
          
        AdministradorService.AccionEliminarEvento().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaPrincipalAdmin') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};

    }]);
