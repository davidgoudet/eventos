Module.service('AdministradorService', ['$q', '$http', function($q, $http) {

    this.AccionCrearEvento = function(evento) {
        return  $http({
          url: "Administrador/AccionCrearEvento",
          data: evento,
          method: 'POST',
          headers: { 'Content-Type': 'multipart/form-data' },
          transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
          }    });
    //    var labels = ["/VistaPrincipalAdmin", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VistaCrearEvento = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Administrador/VistaCrearEvento',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionEditarEvento = function(evento) {
        return  $http({
          url: "Administrador/AccionEditarEvento",
          data: evento,
          method: 'POST',
          headers: { 'Content-Type': 'multipart/form-data' },
          transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
          }    });
    //    var labels = ["/VistaPrincipalAdmin", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VistaEditarEvento = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Administrador/VistaEditarEvento',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionEliminarEvento = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Administrador/AccionEliminarEvento',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaPrincipalAdmin", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.VistaPrincipalAdmin = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Administrador/VistaPrincipalAdmin',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionPrincipalAdmin = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Administrador/AccionPrincipalAdmin',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaPrincipalAdmin", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
}]);