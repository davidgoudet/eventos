Module.config(function ($routeProvider) {
    $routeProvider.when('/VistaReservar', {
                controller: 'VistaReservarController',
                templateUrl: 'app/CUPrincipal/VistaReservar.html'
            }).when('/VistaCredencial', {
                controller: 'VistaCredencialController',
                templateUrl: 'app/CUPrincipal/VistaCredencial.html'
            }).when('/VistaCertificado', {
                controller: 'VistaCertificadoController',
                templateUrl: 'app/CUPrincipal/VistaCertificado.html'
            }).when('/VistaPrincipalUsuario', {
                controller: 'VistaPrincipalUsuarioController',
                templateUrl: 'app/CUPrincipal/VistaPrincipalUsuario.html'
            }).when('/VistaEventoUsuario', {
                controller: 'VistaEventoUsuarioController',
                templateUrl: 'app/CUPrincipal/VistaEventoUsuario.html'
            });
});

Module.controller('VistaReservarController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService',
    function ($scope, $location, $route, flash, CUPrincipalService) {
      $scope.msg = '';
      CUPrincipalService.VistaReservar().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.AccionReservar0 = function() {
          
        CUPrincipalService.AccionReservar().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaReservar') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};

    }]);
Module.controller('VistaCredencialController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService',
    function ($scope, $location, $route, flash, CUPrincipalService) {
      $scope.msg = '';
      CUPrincipalService.VistaCredencial().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.AccionCredencial0 = function() {
          
        CUPrincipalService.AccionCredencial().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaCredencial') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};

    }]);
Module.controller('VistaCertificadoController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService',
    function ($scope, $location, $route, flash, CUPrincipalService) {
      $scope.msg = '';
      CUPrincipalService.VistaCertificado().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.AccionCertificado0 = function() {
          
        CUPrincipalService.AccionCertificado().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaCertificado') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};

    }]);
Module.controller('VistaPrincipalUsuarioController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService',
    function ($scope, $location, $route, flash, CUPrincipalService) {
      $scope.msg = '';
      CUPrincipalService.VistaPrincipalUsuario().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.AccionPrincipalUsuario0 = function() {
          
        CUPrincipalService.AccionPrincipalUsuario().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaPrincipalUsuario') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};
      $scope.VistaCertificado1 = function() {
        $location.path('/VistaCertificado');
      };
      $scope.VistaCredencial2 = function() {
        $location.path('/VistaCredencial');
      };
      $scope.VistaReservar3 = function() {
        $location.path('/VistaReservar');
      };

    }]);
Module.controller('VistaEventoUsuarioController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService',
    function ($scope, $location, $route, flash, CUPrincipalService) {
      $scope.msg = '';
      CUPrincipalService.VistaEventoUsuario().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.AccionEventoUsuario0 = function() {
          
        CUPrincipalService.AccionEventoUsuario().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaEventoUsuario') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};

    }]);
