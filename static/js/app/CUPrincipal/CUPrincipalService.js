Module.service('CUPrincipalService', ['$q', '$http', function($q, $http) {

    this.AccionReservar = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/AccionReservar',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaPrincipalUsuario", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.VistaReservar = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/VistaReservar',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionCredencial = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/AccionCredencial',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaPrincipalUsuario", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.VistaCredencial = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/VistaCredencial',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionCertificado = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/AccionCertificado',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaPrincipalUsuario", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.AccionPrincipalUsuario = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/AccionPrincipalUsuario',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaPrincipalUsuario", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.VistaCertificado = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/VistaCertificado',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionEventoUsuario = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/AccionEventoUsuario',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaEventoUsuario", "/VistaPrincipalUsuario", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.VistaPrincipalUsuario = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/VistaPrincipalUsuario',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VistaEventoUsuario = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'CUPrincipal/VistaEventoUsuario',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

}]);