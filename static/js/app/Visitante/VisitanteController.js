Module.config(function ($routeProvider) {
    $routeProvider.when('/VistaPrincipal', {
                controller: 'VistaPrincipalController',
                templateUrl: 'app/Visitante/VistaPrincipal.html'
            }).when('/VistaRegistro', {
                controller: 'VistaRegistroController',
                templateUrl: 'app/Visitante/VistaRegistro.html'
            }).when('/VistaInicioSesion', {
                controller: 'VistaInicioSesionController',
                templateUrl: 'app/Visitante/VistaInicioSesion.html'
            });
});

Module.controller('VistaPrincipalController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService', 'VisitanteService',
    function ($scope, $location, $route, flash, CUPrincipalService, VisitanteService) {
      $scope.msg = '';
      VisitanteService.VistaPrincipal().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.AccionPrincipal0 = function() {
          
        VisitanteService.AccionPrincipal().then(function (object) {
          var msg = object.data["msg"];
          if (msg) flash(msg);
          var label = object.data["label"];
          if (label == '/VistaPrincipal') {
              $route.reload();
          } else {
              $location.path(label);
          }
        });};
      $scope.VistaRegistro1 = function() {
        $location.path('/VistaRegistro');
      };

    }]);
Module.controller('VistaRegistroController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService', 'VisitanteService',
    function ($scope, $location, $route, flash, CUPrincipalService, VisitanteService) {
      $scope.msg = '';
      $scope.registro = {};

      VisitanteService.VistaRegistro().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.registroSubmitted = false;
      $scope.AccionRegistro0 = function(isValid) {
        $scope.registroSubmitted = true;
        if (isValid) {
          
          VisitanteService.AccionRegistro($scope.registro).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              if (label == '/VistaRegistro') {
                  $route.reload();
              } else {
                  $location.path(label);
              }
          });
        }
      };

    }]);
Module.controller('VistaInicioSesionController', 
   ['$scope', '$location', '$route', 'flash', 'CUPrincipalService', 'VisitanteService',
    function ($scope, $location, $route, flash, CUPrincipalService, VisitanteService) {
      $scope.msg = '';
      $scope.inicioSesion = {};

      VisitanteService.VistaInicioSesion().then(function (object) {
        $scope.res = object.data;
        for (var key in object.data) {
            $scope[key] = object.data[key];
        }
        if ($scope.logout) {
            $location.path('/');
        }
      });
      $scope.inicioSesionSubmitted = false;
      $scope.AccionInicioSesion0 = function(isValid) {
        $scope.inicioSesionSubmitted = true;
        if (isValid) {
          
          VisitanteService.AccionInicioSesion($scope.inicioSesion).then(function (object) {
              var msg = object.data["msg"];
              if (msg) flash(msg);
              var label = object.data["label"];
              if (label == '/VistaInicioSesion') {
                  $route.reload();
              } else {
                  $location.path(label);
              }
          });
        }
      };

    }]);
