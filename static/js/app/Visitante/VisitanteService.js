Module.service('VisitanteService', ['$q', '$http', function($q, $http) {

    this.AccionPrincipal = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Visitante/AccionPrincipal',
          method: 'GET',
          params: args
        });
    //    var labels = ["/VistaPrincipal", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };
    this.VistaPrincipal = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Visitante/VistaPrincipal',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionRegistro = function(registro) {
        return  $http({
          url: "Visitante/AccionRegistro",
          data: registro,
          method: 'POST',
        });
    //    var labels = ["/VistaInicioSesion", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VistaRegistro = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Visitante/VistaRegistro',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.AccionInicioSesion = function(inicioSesion) {
        return  $http({
          url: "Visitante/AccionInicioSesion",
          data: inicioSesion,
          method: 'POST',
        });
    //    var labels = ["/VistaPrincipalUsuario", ];
    //    var res = labels[0];
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

    this.VistaInicioSesion = function(args) {
        if(typeof args == 'undefined') args={};
        return $http({
          url: 'Visitante/VistaInicioSesion',
          method: 'GET',
          params: args
        });
    //    var res = {};
    //    var deferred = $q.defer();
    //    deferred.resolve(res);
    //    return deferred.promise;
    };

}]);