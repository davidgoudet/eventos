// Creación del módulo de la aplicación
var Module = angular.module('', ['ngRoute', 'ngAnimate', 'flash']);
Module.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
                controller: 'VistaPrincipalController',
                templateUrl: 'app/Visitante/VistaPrincipal.html'
            });
});
Module.controller('Controller_',  ['$scope', '$http', '$location',
function($scope) {
    $scope.title = "Modelo";
}]);
Module.directive('file', function () {
    return {
        restrict: 'A',
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});
